package net.iqbusiness.services;

import net.iqbusiness.entities.User;

import java.util.List;

public interface UserService {

    public User saveUser(User user);
    public List<User> getUsers();
}
