# Registration App
An Angularjs spring-boot application that deals with a user registration and viewing of users all backed by a in-memory database 

#project pre-requisites

You need JAVA 8, Maven, Docker, Node&NPM and Angular Cli to be able to successfully run this application

## starting the app manually

step1:

start your backend application

>cd backend
>
>mvn clean install
>
>mvn spring-boot:run

step2:

start up the angular front end

in another terminal

>cd frontend
>
>npm install
>
>ng serve

step3:

The App is available here: http://localhost:4200

The API is available here: http://localhost:8080

## starting the app using docker-compose

step1:

start backend application

>cd backend
>
>mvn clean install
>
>docker-compose up

step2:

>cd frontend
>
>docker-compose up --build

start frontend application


## Other commands

To bring down the docker containers run 

>docker-compose down

